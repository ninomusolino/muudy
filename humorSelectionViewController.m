//
//  humorSelectionViewController.m
//  Muudy
//
//  Created by Antonino Francesco Musolino on 11/05/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import "humorSelectionViewController.h"

@interface humorSelectionViewController ()

@end

@implementation humorSelectionViewController
@synthesize myDelegate,scoreX,scoreY,image,imageContainer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
    image.userInteractionEnabled = YES;
    [image addGestureRecognizer:tap];
    self.navigationItem.hidesBackButton = YES;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)imageTapped:(UITapGestureRecognizer *) gestureRecognizer
{
    //TODO
    
     CGPoint pointy =[gestureRecognizer locationInView:imageContainer];
     xScore = ((pointy.x * 2)/image.bounds.size.width)-1;
     yScore = ((pointy.y * 2)/image.bounds.size.height)-1;
     yScore = -yScore;
     //NSLog(@"point X = %f",  xScore);
     //NSLog(@"point Y = %f",  yScore);
     if([self.myDelegate respondsToSelector:@selector(secondViewControllerDismissed:withY:)])
     {
         [self.myDelegate secondViewControllerDismissed:xScore withY:yScore];
     }
     [self.navigationController popViewControllerAnimated:YES];
}


@end
