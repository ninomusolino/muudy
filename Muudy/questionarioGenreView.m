//
//  questionarioDummyView.m
//  Muudy
//
//  Created by Antonino Francesco Musolino on 11/03/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import "questionarioGenreView.h"

@interface questionarioGenreView ()

@end

@implementation questionarioGenreView


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSString *query = @"SELECT g.Genere FROM Genere g";
    tableData = [[NSArray alloc] initWithObjects:@"Genere",nil];
    //MAGARI MESSO SU GCD
    _tableView.delegate = self;
    _tableView.dataSource = self;
    genre = [MuudyDatabaseManager recoverDataFromDb:query :tableData];
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    _tableView.separatorColor = [UIColor whiteColor];
    [_tableView reloadData];
	// Do any additional setup after loading the view.
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
   [self.navigationItem setHidesBackButton:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)next:(id)sender
{
    [self performSegueWithIdentifier:@"next" sender:self];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [genre count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [_tableView
                             dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if(cell==nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        [cell setBackgroundColor:[UIColor colorWithRed:.8 green:.8 blue:1 alpha:1]];
    }
    
    cell.textLabel.text = [[genre objectAtIndex:indexPath.row] objectForKey:@"Genere"];
    //cell.imageView.image = [UIImage imageNamed:@"itunes10_512_circle.png"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    gen = [[genre objectAtIndex:indexPath.row] objectForKey:@"Genere"];
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
    //activityview.hidden = NO;
    [self performSegueWithIdentifier:@"selectHumor" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"selectHumor"])
    {
         humorSelectionViewController *svc = (humorSelectionViewController *)segue.destinationViewController;
         svc.myDelegate = self;
    }
}

-(void) secondViewControllerDismissed:(float)xScore withY:(float)yScore
{
    //POSSO SALVARE LE PREFERENZE direttamente nella tabella GENERE dove ho i top generi
    NSString *query = [NSString stringWithFormat:@"UPDATE Song SET ScoreX = %f , ScoreY = %f WHERE Genre = '%@'",xScore,yScore,gen];
    NSString *query2 = [NSString stringWithFormat:@"UPDATE Genere SET ScoreX = %f , ScoreY = %f WHERE Genere = '%@'",xScore,yScore,gen];
    
    
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_sync(concurrentQueue,^{ [MuudyDatabaseManager executeQuery:query];});
    dispatch_sync(concurrentQueue,^{ [MuudyDatabaseManager executeQuery:query2]; });

}


@end
