//
//  main.m
//  Muudy
//
//  Created by Antonino Francesco Musolino on 08/05/13.
//  Copyright (c) 2013 Antonino Francesco Musolino. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
