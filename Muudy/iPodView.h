//
//  iPodView.h
//  Muudy
//
//  Created by fabiano femia on 12/03/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "MuudyDatabaseManager.h"
#import "humorSelectionViewController.h"
#import "MuudyIpodHandler.h"

@class humorSelectionViewController;

@interface iPodView : UIViewController<AVAudioPlayerDelegate,MPMediaPickerControllerDelegate,SecondDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UIView         *viewVolume;
    IBOutlet UILabel        *labVol;
    IBOutlet UISlider       *volumeSlider;
    MPMusicPlayerController *musicPlayer;
    NSArray                 *songsArray;
    int                     ind;
    BOOL                    playing;
    AVAudioPlayer           *appSoundPlayer;
    IBOutlet UILabel        *artistLabel;
    IBOutlet UILabel        *titleLabel;
    IBOutlet UIImageView    *sfondo;
    IBOutlet UIImageView    *sfondoBlur;
    NSString                *titolo;
    NSString                *artista;
    NSString                *album;
    IBOutlet UISlider       *sliderDur;
    IBOutlet UILabel        *currentTimeLabel;
    NSTimer                 *sliderTimer;
    IBOutlet UILabel        *durationLabel;
    BOOL                    durNegativa;
    int                     totDur;
    int                     calcDur;
    IBOutlet UIButton       *playPauseBtn;
    IBOutlet UIImageView    *imagePulse2;
    IBOutlet UILabel        *labelCanzoniTot;
    IBOutlet UILabel        *labelCanzoneAtt;
    float                   volVal;
    NSTimer                 *timerVol;
    float                   oldVol;
    MPMediaItemArtwork      *artwork;

}

@property (nonatomic)NSArray *songsArray;


-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag;
-(IBAction)modifyHumor:(id)sender;
@end
