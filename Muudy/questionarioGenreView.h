//
//  questionarioDummyView.h
//  Muudy
//
//  Created by Antonino Francesco Musolino on 11/03/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MuudyDatabaseManager.h"
#import "humorSelectionViewController.h"

@class humorSelectionViewController;

@interface questionarioGenreView : UIViewController<UITableViewDelegate, UITableViewDataSource,SecondDelegate, UIGestureRecognizerDelegate>
{
    NSArray                     *genre;
    NSArray                     *tableData;
    NSString                    *gen;
}

@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (nonatomic, retain) IBOutlet UITableView *tableView;

-(IBAction)next:(id)sender;

@end
