//
//  Data.m
//  Muudy
//
//  Created by Antonino Francesco Musolino on 12/05/13.
//  Copyright (c) 2013 Antonino Francesco Musolino. All rights reserved.
//

#import "MuudyDatabaseManager.h"
#import "SVProgressHUD.h"

static sqlite3 *database = nil;
static sqlite3_stmt *selectstmt = nil;
static sqlite3_stmt *addStmt = nil;
static sqlite3_stmt *updateStmt = nil;

@implementation MuudyDatabaseManager

@synthesize lista;

// Ritorna la dimensione della lista (n° di elementi letti dal db)
- (unsigned)getSize {
    return (int)[lista count];
}

// Ritorna l'oggetto ad una data posizione
- (id)objectAtIndex:(unsigned)index {
    return [lista objectAtIndex:index];
}

/* ESEMPIO 
 NSString * query = @” SELECT nomeutente, username, password FROM miaTabella”;
 NSArray * arrayQuery = [[NSArray alloc] initWithObjects:@”nomeutente”,@”username”,@”password”,nil];
 NSArray * arrayElementr = [self caricaValoriDaDBStandard:query :arrayQuery];
 */

+(NSArray *)recoverDataFromDb:(NSString *)query :(NSArray *)arrayKey
{
	
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString *dbPath = [documentsDir stringByAppendingPathComponent:@"muudy.sqlite"];
	if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
    {
		// query che ricava i valori
		const char *sql = [query UTF8String];;

		
		// lista temporanea
		NSMutableArray *listaTemp = [[NSMutableArray alloc] init];
		
		if(sqlite3_prepare_v2(database, sql, -1, &selectstmt, NULL) == SQLITE_OK)
		{

			while(sqlite3_step(selectstmt) == SQLITE_ROW)
			{
                				
				// Oggetto che contiene i vari elementi
				NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
				// ricaviamo i valori letti dalla query
				for (int k=0; k<[arrayKey count]; k++)
				{
					//NSMutableString *contenuto = [NSMutableString alloc];
					if (sqlite3_column_text(selectstmt, k) == NULL)
					{
						
						//NSMutableString *contenuto = [[NSMutableString alloc] initWithFormat:@""];
						//NSLog(@" %@ \n", contenuto);
						[dictionary setObject:@"" forKey:[arrayKey objectAtIndex:k]];
						//[contenuto release];
					}
					else
					{
						NSMutableString *contenuto = [[NSMutableString alloc] initWithUTF8String:(char *)sqlite3_column_text(selectstmt, k)];
						//NSLog(@" %@ \n", contenuto);
						[dictionary setObject:contenuto forKey:[arrayKey objectAtIndex:k]];
					}
					
                    
				}
				
				[listaTemp addObject:dictionary];
            }
        }
        else
        {
            [SVProgressHUD showErrorWithStatus:@"Modifiche non apportate"];
            NSLog(@"Failed to prepare statement '%s'",sqlite3_errmsg(database));
        }
		//NSLog(@"%@", listaTemp);
		sqlite3_close(database);
		return listaTemp;
	}
	else
	{
		sqlite3_close(database);
		NSMutableArray *listaTemp = [[NSMutableArray alloc] init];
		return listaTemp;
	}
}

+ (int)executeQuery:(NSString *)query
{
    
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString *dbPath = [documentsDir stringByAppendingPathComponent:@"muudy.sqlite"];
	NSLog(@"APRO LA CONNESSIONE");
	if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
        NSLog(@"Connessione aperta");
        const char *sql = [query UTF8String];

		char *err;
        if(sqlite3_exec(database, sql, NULL, NULL, &err) != SQLITE_OK)
        {
            NSAssert1(0, @"Could not update table '%s'", sqlite3_errmsg(database));
            sqlite3_close(database);
        }
        else
        {
            NSLog(@"Table Updated G");
            sqlite3_close(database);
            return 1;
        }
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"hideActivityViewG" object:self];
	}
	else
		sqlite3_close(database);
    return 0;
}

//INSERIMENTO INIZIALE SOLAMENTE           
//(NSArray *)listaM :(UIProgressView *)ort

/*
 0,14/25  0,14/36 0,65/141
 0,0056    0,0038   0,0046
 */

+(void)insertSongInit:(NSArray *) wrap
{
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString *dbPath = [documentsDir stringByAppendingPathComponent:@"muudy.sqlite"];
	NSLog(@"APRO LA CONNESSIONE");
	if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
        if(addStmt == nil)
        {
            static char *sql = "INSERT INTO Song (Titolo,Artist,Album,Genre) VALUES (?,?,?,?)";
            if(sqlite3_prepare_v2(database, sql, -1, &addStmt, NULL)!= SQLITE_OK)
            {
                NSAssert1(0, @"ERROR: failed to prepare statement with error '%s'.", sqlite3_errmsg(database));
            }
        }
        int tot = (int)[wrap count]*2;
        sqlite3_exec(database, "BEGIN;", 0, 0, 0);
        for (int i = 0; i<[wrap count]; i++)
        {
            NSMutableDictionary *orf = [wrap objectAtIndex:i];
            NSString * tit = [orf objectForKey:@"Titolo"]; 
            NSString * art = [orf objectForKey:@"Artista"];
            NSString * alb = [orf objectForKey:@"Album"];
            NSString * gen = [orf objectForKey:@"Genere"];
            
            //inserisco
            sqlite3_bind_text(addStmt, 1, [tit UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 2, [art UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 3, [alb UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 4, [gen UTF8String], -1, SQLITE_TRANSIENT);
            if (sqlite3_step(addStmt) == SQLITE_DONE)
            {
                //NSLog(@"Aggiunto record '%@', '%@', '%@', '%@'",tit,art,alb,gen);
            } else {
                NSLog(@"Failed to add contact '%s'",sqlite3_errmsg(database));
            }
            sqlite3_reset(addStmt);
            
            NSNumber *progr = [[NSNumber alloc] initWithFloat:((float)i /(float)tot)];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"incrementaProgressView" object:progr];

        }
        NSLog(@"TABELLA INIZIALIZZATA");
        sqlite3_exec(database, "END;", 0, 0, 0);
        sqlite3_finalize(addStmt);
        sqlite3_close(database);
        [self executeQuery:@"INSERT INTO Genere SELECT s.Genre, s.ScoreX, s.ScoreY FROM Song s GROUP BY s.Genre HAVING COUNT(s.Genre)>10"];
        [self executeQuery:@"INSERT INTO Artista SELECT s.Artist, s.Genre, s.ScoreX, s.ScoreY FROM Song s GROUP BY s.Artist HAVING COUNT(s.Artist)>10"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"vaiQuest" object:nil];
    }
}

+(void)updateScore:(NSArray *)listaM
{
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString *dbPath = [documentsDir stringByAppendingPathComponent:@"muudy.sqlite"];
	NSLog(@"APRO LA CONNESSIONE");
	if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
        static char *sql = "UPDATE Song SET ScoreX = ? , ScoreY = ? WHERE Titolo = ?";
        if(sqlite3_prepare_v2(database, sql, -1, &updateStmt, NULL)!= SQLITE_OK)
        {
            NSAssert1(0, @"ERROR: failed to prepare statement with error '%s'.", sqlite3_errmsg(database));
        }
        sqlite3_exec(database, "BEGIN;", 0, 0, 0);
        for (int i = 0; i<[listaM count]; i++)
        {
            NSMutableDictionary *orf = [listaM objectAtIndex:i];
            NSString * tit = [orf objectForKey:@"Titolo"];
            double sx = [[orf valueForKey:@"xScore"] floatValue];
            double sy = [[orf valueForKey:@"yScore"] floatValue];
            //inserisco
            sqlite3_bind_double(updateStmt, 1, sx);
            sqlite3_bind_double(updateStmt, 2, sy);
            sqlite3_bind_text(updateStmt, 3, [tit UTF8String], -1, SQLITE_TRANSIENT);
            if (SQLITE_DONE != sqlite3_step(updateStmt))
            {
                NSAssert1(0, @"Error while updating. '%s'", sqlite3_errmsg(database));
            }
            sqlite3_reset(updateStmt);
        }
        NSLog(@"TABELLA AGGIORNATA");
        sqlite3_exec(database, "END;", 0, 0, 0);
        sqlite3_finalize(updateStmt);
        sqlite3_close(database);
    }
}


+(void)updateArtist:(NSArray *) listaM
{
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	NSString *dbPath = [documentsDir stringByAppendingPathComponent:@"muudy.sqlite"];
	NSLog(@"APRO LA CONNESSIONE");
	if (sqlite3_open([dbPath UTF8String], &database) == SQLITE_OK)
	{
        static char *sql = "UPDATE Song SET ScoreX = ? , ScoreY = ? WHERE Titolo = ? AND Artist = ?";
        if(sqlite3_prepare_v2(database, sql, -1, &updateStmt, NULL)!= SQLITE_OK)
        {
            NSAssert1(0, @"ERROR: failed to prepare statement with error '%s'.", sqlite3_errmsg(database));
        }
        sqlite3_exec(database, "BEGIN;", 0, 0, 0);
        NSLog(@"COUNT %lu",(unsigned long)[listaM count]);
        for (int i = 0; i<[listaM count]; i++)
        {
            NSMutableDictionary *orf = [listaM objectAtIndex:i];
            NSString * tit = [orf objectForKey:@"Titolo"] ;
            NSString * art = [orf objectForKey:@"Artista"];
            double sx = [[orf valueForKey:@"xScore"] floatValue];
            double sy = [[orf valueForKey:@"yScore"] floatValue];
            //NSLog(@"HO QUESTI VALORI '%@', '%@', '%f', '%f'",tit,art,sx,sy);
            //inserisco
            sqlite3_bind_double(updateStmt, 1, sx);
            sqlite3_bind_double(updateStmt, 2, sy);
            sqlite3_bind_text(updateStmt, 3, [tit UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 4, [art UTF8String], -1, SQLITE_TRANSIENT);
            if (SQLITE_DONE != sqlite3_step(updateStmt))
            {
                NSAssert1(0, @"Error while updating. '%s'", sqlite3_errmsg(database));
            }
            sqlite3_reset(updateStmt);
        }
        NSLog(@"TABELLA INIZIALIZZATA");
        sqlite3_exec(database, "END;", 0, 0, 0);
        sqlite3_finalize(updateStmt);
        sqlite3_close(database);
    }
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"hideActivityView" object:self];
}

+(NSArray *)loadMusicDatabase
{
    MPMediaPropertyPredicate *noPodcastPredicate =[MPMediaPropertyPredicate predicateWithValue:[NSNumber numberWithInteger:MPMediaTypeMusic] forProperty: MPMediaItemPropertyMediaType];
    MPMediaQuery *myArtistQuery = [[MPMediaQuery alloc] init];
    [myArtistQuery addFilterPredicate:noPodcastPredicate];
     return [myArtistQuery items];
}

@end
