//
//  iPodView.m
//  Muudy
//
//  Created by fabiano femia on 12/03/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import "iPodView.h"
#import <QuartzCore/QuartzCore.h>
#import "FXBlurView.h"
#import <AudioToolbox/AudioToolbox.h>
#import <MediaPlayer/MediaPlayer.h>
#import "TSMessage.h"

#import <MediaPlayer/MPNowPlayingInfoCenter.h>


@interface iPodView ()
@property (nonatomic, weak) IBOutlet FXBlurView *blurView;
@property (nonatomic, retain) IBOutlet UIButton *playPauseBtn;
@end

@implementation iPodView
@synthesize songsArray,playPauseBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIImage *sliderLeftTrackImage = [[UIImage imageNamed: @"max.png"] stretchableImageWithLeftCapWidth: 0 topCapHeight: 4];
    UIImage *sliderRightTrackImage = [[UIImage imageNamed: @"min.png"] stretchableImageWithLeftCapWidth: 0 topCapHeight: 4];
    [sliderDur setThumbImage:[UIImage imageNamed:@"thumb_15_2.png"] forState:UIControlStateNormal];
    [sliderDur setThumbImage:[UIImage imageNamed:@"thumb_15_2.png"] forState:UIControlStateHighlighted];
    [sliderDur setMinimumTrackImage: sliderLeftTrackImage forState: UIControlStateNormal];
    [sliderDur setMaximumTrackImage: sliderRightTrackImage forState: UIControlStateNormal];
    UIImage *sliderLeftTrackImage2 = [[UIImage imageNamed: @"maxVol.png"] stretchableImageWithLeftCapWidth: 0 topCapHeight: 1];
    UIImage *sliderRightTrackImage2 = [[UIImage imageNamed: @"minVol.png"] stretchableImageWithLeftCapWidth: 0 topCapHeight: 1];
    [volumeSlider setThumbImage:[UIImage imageNamed:@"thumb_15.png"] forState:UIControlStateNormal];
    [volumeSlider setThumbImage:[UIImage imageNamed:@"thumb_15.png"] forState:UIControlStateHighlighted];
    [volumeSlider setMinimumTrackImage: sliderLeftTrackImage2 forState: UIControlStateNormal];
    [volumeSlider setMaximumTrackImage: sliderRightTrackImage2 forState: UIControlStateNormal];
    self.blurView.blurRadius = 20;
    durNegativa = TRUE;
    float vol = [[AVAudioSession sharedInstance] outputVolume];
    NSLog(@"output volume: %0.f", 100*(vol+FLT_MIN));
    float vol2 = 100*(vol+FLT_MIN);
    oldVol = vol2;
    [volumeSlider setValue:vol2];
    viewVolume.layer.cornerRadius = 5;
    viewVolume.layer.masksToBounds = YES;
    sfondo.layer.cornerRadius = 5;
    sfondo.layer.masksToBounds = YES;
    sfondo.layer.borderColor = [UIColor colorWithRed:255 green:255 blue:255 alpha:0.2].CGColor;
    sfondo.layer.borderWidth = 3.0f;
    ind = 0;
    
    //CARICA CANZONI
    MPMediaPropertyPredicate *noPodcastPredicate =[MPMediaPropertyPredicate predicateWithValue:[NSNumber numberWithInteger:MPMediaTypeMusic] forProperty: MPMediaItemPropertyMediaType];
    MPMediaQuery *myArtistQuery = [[MPMediaQuery alloc] init];
    [myArtistQuery addFilterPredicate:noPodcastPredicate];
    songsArray = [myArtistQuery items];
    NSLog(@"%lu",(unsigned long)[songsArray count]);
    
    
    //tasti esterni
    [[UIApplication sharedApplication]beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
    
    [playPauseBtn setBackgroundImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(animPulse) userInfo:nil repeats:YES];
    [appSoundPlayer stop];
    

    NSInteger p = [songs count];
    labelCanzoniTot.text = [NSString stringWithFormat:@"%li", (long)p];
    volVal = 6.25;
    [self readAudioVol];
    
    [self createPlayList];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}
-(IBAction)readAudioVol{
    timerVol = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(readAudio) userInfo:nil repeats:YES];
}

-(void)volumeUP
{
    viewVolume.alpha = 1;
    volumeSlider.value = volumeSlider.value + volVal;
    NSString *volSlVal = [NSString stringWithFormat:@"%0.f%@", volumeSlider.value, @"%"];
    appSoundPlayer.volume = volumeSlider.value/100l;
    [labVol setText:volSlVal];
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(hideViewVolume) userInfo:nil repeats:NO];
}

-(void)volumeDOWN{
    viewVolume.alpha = 1;
    volumeSlider.value = volumeSlider.value - volVal;
    NSString *volSlVal = [NSString stringWithFormat:@"%0.f%@", volumeSlider.value, @"%"];
    appSoundPlayer.volume = volumeSlider.value/100l;
    [labVol setText:volSlVal];
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(hideViewVolume) userInfo:nil repeats:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createPlayList
{
    //recupero il titolo della canzone all'indice ind
    NSString * titol = [[[[MuudyIpodHandler sharedInstance]getSongs] objectAtIndex:ind]objectForKey:@"Titolo"];
    MPMediaPropertyPredicate *predicateFilterByTitle =[MPMediaPropertyPredicate predicateWithValue:titol forProperty:MPMediaItemPropertyTitle];
    MPMediaQuery *myArtistQuery = [[MPMediaQuery alloc]init] ;
    [myArtistQuery addFilterPredicate:predicateFilterByTitle];
    //contiene una sola canzone
    songsArray = [myArtistQuery items];
    MPMediaItem *song = [songsArray objectAtIndex:0];
    NSURL *songURL = [song valueForProperty:MPMediaItemPropertyAssetURL];
    NSError *error;
    appSoundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:songURL error:&error];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    appSoundPlayer.delegate = self;
    [appSoundPlayer prepareToPlay];
    [appSoundPlayer play];
    playing = true;
    [self changeSong:song];
    [self infoControlCenter];
}

#pragma mark Music control________________________________

-(IBAction)nextSong:(id)sender
{
    NSInteger p = [[[MuudyIpodHandler sharedInstance]getSongs] count];
    NSLog(@"%ld", (long)p);
    if (ind==p-1)
    {
        ind = 0;
        NSLog(@"INDICE = %d", ind);
        [self createPlayList];
    }
    else
    {
        ind++;
        NSLog(@"INDICE = %d", ind);
        [self createPlayList];
    }
    [self infoControlCenter];
    labelCanzoneAtt.text = [NSString stringWithFormat:@"%d", ind+1];
}

-(IBAction)previousSong:(id)sender
{
   // NSInteger p = [songs count];
    //NSLog(@"%ld", (long)p);
    if (ind==0)
    {
        ind=(int)[[[MuudyIpodHandler sharedInstance]getSongs] count]-1;
        //NSLog(@"INDICE = %lu", ind);
        [self createPlayList];
    }
    else
    {
        ind--;
        //NSLog(@"INDICE = %lu", ind);
        [self createPlayList];
    }
    [self infoControlCenter];
    labelCanzoneAtt.text = [NSString stringWithFormat:@"%d", ind+1];
}

- (IBAction) playOrPauseMusic:(id)sender
{
    UIImage *imgBgPause = [UIImage imageNamed:@"pause.png"];
    UIImage *imgBgPlay = [UIImage imageNamed:@"play.png"];
    if (!playing)
    {
        [playPauseBtn setBackgroundImage:imgBgPause forState:UIControlStateNormal];
        [appSoundPlayer prepareToPlay];
        [appSoundPlayer play];
        //NSLog(@"PLAY");
        playing = true;
        [self infoControlCenter];
    }
    else
    {
        //NSLog(@"PAUSE");
        [playPauseBtn setBackgroundImage:imgBgPlay forState:UIControlStateNormal];
        [appSoundPlayer pause];
        playing = false;
    }
    
}

-(void)infoControlCenter
{
    Class playingInfoCenter = NSClassFromString(@"MPNowPlayingInfoCenter");
    
    if (playingInfoCenter) {
        NSMutableDictionary *songInfo = [[NSMutableDictionary alloc] init];
        MPMediaItemArtwork *albumArt = [[MPMediaItemArtwork alloc] initWithImage:sfondo.image];
        [songInfo setObject:titolo forKey:MPMediaItemPropertyTitle];
        [songInfo setObject:artista forKey:MPMediaItemPropertyArtist];
        [songInfo setObject:album forKey:MPMediaItemPropertyAlbumTitle];
        [songInfo setObject:albumArt forKey:MPMediaItemPropertyArtwork];
        [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:songInfo];
    }
}

#pragma mark CAMBIAMENTI GRAFICI

-(void)changeSong: (MPMediaItem *) song
{
    titolo = [song valueForProperty: MPMediaItemPropertyTitle];
    artista = [song valueForProperty: MPMediaItemPropertyArtist];
    album = [song valueForProperty: MPMediaItemPropertyAlbumTitle];
    [artistLabel setText:artista];
    [titleLabel setText:titolo];
    //SEEK SLIDER
    sliderDur.minimumValue = 0.0f;
    sliderDur.value=0.0f;
    sliderDur.maximumValue = appSoundPlayer.duration;
    currentTimeLabel.text = [NSString stringWithFormat:@"%d:%02d", 0, 0, nil];
    [sliderTimer invalidate];
    sliderTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
    durationLabel.text = [NSString stringWithFormat:@"%d:%02d",(int)self->appSoundPlayer.duration / 60, (int)self->appSoundPlayer.duration % 60];
    
    artwork = [song valueForProperty: MPMediaItemPropertyArtwork];
    UIImage *artworkImage = [artwork imageWithSize: sfondo.bounds.size];
    //sfondo.contentMode = UIViewContentModeScaleAspectFit;
    if (artworkImage)
    {
        sfondo.image = artworkImage;
        sfondoBlur.image = sfondo.image;
    }
    else
    {
        sfondo.image = [UIImage imageNamed: @"bg_blankArt.png"];
        sfondoBlur.image = sfondo.image;
    }
}

- (void)updateSlider
{
    sliderDur.value = appSoundPlayer.currentTime;
    currentTimeLabel.text = [NSString stringWithFormat:@"%d:%02d", (int)self->appSoundPlayer.currentTime / 60, (int)appSoundPlayer.currentTime % 60, nil];
    if (durNegativa==TRUE) {
        totDur = appSoundPlayer.duration - appSoundPlayer.currentTime;
        durationLabel.text = [NSString stringWithFormat:@"%@%d:%02d", @"-", (int)self->totDur / 60, (int)self->totDur % 60];
    }
}

-(IBAction)changeTime_valueChanged
{
    if (durNegativa==TRUE)
    {
        totDur = sliderDur.maximumValue - sliderDur.value;
        durationLabel.text = [NSString stringWithFormat:@"%@%d:%02d", @"-", (int)self->totDur / 60, (int)self->totDur % 60];
    }
    currentTimeLabel.text = [NSString stringWithFormat:@"%d:%02d", (int)self->sliderDur.value / 60, (int)sliderDur.value % 60, nil];
}

-(IBAction)changeTime_stopTimer{
   // NSLog(@"PRESS");
    [sliderTimer invalidate];
    currentTimeLabel.text = [NSString stringWithFormat:@"%d:%02d", (int)self->appSoundPlayer.currentTime / 60, (int)appSoundPlayer.currentTime % 60, nil];
}

-(IBAction)changeTime_detectTime{
    appSoundPlayer.currentTime = sliderDur.value;
    sliderTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSlider) userInfo:nil repeats:YES];
}

-(void)animPulse{
    if (imagePulse2.alpha==1) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration: 1];
        imagePulse2.alpha = 0;
        [UIView commitAnimations];
    }else{
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration: 1];
        imagePulse2.alpha = 1;
        [UIView commitAnimations];
    }
}

-(void)remoteControlReceivedWithEvent:(UIEvent *)event
{
    if(event.type == UIEventTypeRemoteControl)
    {
        switch (event.subtype)
        {
            case UIEventSubtypeRemoteControlPause:
            {
                //NSLog(@"PREMUTO PLAY O PAUSE");
                [self playOrPauseMusic:nil];
            }
                break;
            case UIEventSubtypeRemoteControlPlay:
            {
                //NSLog(@"PREMUTO PLAY O PAUSE");
                [self playOrPauseMusic:nil];
            }
                break;
            case UIEventSubtypeRemoteControlPreviousTrack:
            {
                //NSLog(@"PREMUTO PREV");
                [self previousSong:nil];
            }
                break;
            case UIEventSubtypeRemoteControlNextTrack:
                [self nextSong:nil];
                break;
            default:
                break;
        }
    }
}

-(BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    if (flag)
    {
        [sliderTimer invalidate];
    }
    if (ind < songs.count)
    {
        //AGGIORNARE IL PLAYCOUNT
        NSString *temp = [[songs objectAtIndex:ind]objectForKey:@"Titolo"];
        NSString *query = [NSString stringWithFormat:@"UPDATE Song SET Playcount = Playcount +1 WHERE Titolo ='%@' ",temp];
        [MuudyDatabaseManager executeQuery:query];
        [self nextSong:nil];
    }
    else
    {
        //AGGIORNARE IL PLAYCOUNT
        NSString *temp = [[songs objectAtIndex:ind]objectForKey:@"Titolo"];
        NSString *query = [NSString stringWithFormat:@"UPDATE Song SET Playcount = Playcount +1 WHERE Titolo = '%@' ",temp];
        [MuudyDatabaseManager executeQuery:query];
        [appSoundPlayer stop];
        playing = false;
    }
}
- (IBAction)changeDurationLabel{
    //durationLabel.text = [NSString stringWithFormat:@"%d:%02d",(int)self->appSoundPlayer.duration / 60, (int)self->appSoundPlayer.duration % 60];
    //NSLog(@"%0.f", musicPlayer.volume);
    
    if (durNegativa==FALSE) {
        //NSLog(@"durNegativa settata su TRUE");
        durNegativa = TRUE;
    }else{
        //NSLog(@"durNegativa settata su FALSE");
        durNegativa = FALSE;
    }
}

-(IBAction)audioReadInvalidate{
    [timerVol invalidate];
}
-(IBAction)startObs{
    /*AudioSessionInitialize(NULL, NULL, NULL, NULL);
    AudioSessionSetActive(YES);
    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(volumeDidChange:) name:@"AVSystemController_SystemVolumeDidChangeNotification" object:nil];*/
    
}

-(void)readAudio{
    float vol = [[AVAudioSession sharedInstance] outputVolume];
    float vol2 = 100*(vol+FLT_MIN);
    if (vol2!=oldVol) {
        viewVolume.alpha = 1;
        [volumeSlider setValue:vol2];
        NSString *volSlVal = [NSString stringWithFormat:@"%0.f%@", volumeSlider.value, @"%"];
        [labVol setText:volSlVal];
        oldVol = vol2;
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(hideViewVolume) userInfo:nil repeats:NO];
    }
}


-(IBAction)updateVolume{
    viewVolume.alpha = 1;
    NSString *volSlVal = [NSString stringWithFormat:@"%0.f%@", volumeSlider.value, @"%"];
    appSoundPlayer.volume = volumeSlider.value/100;
    [labVol setText:volSlVal];
}
-(IBAction)hideViewVolume{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration: 0.6];
    viewVolume.alpha = 0;
    [UIView commitAnimations];
}

-(IBAction)stoMusic{
    [appSoundPlayer stop];
}

//MODIFICA HUMOR

-(IBAction)modifyHumor:(id)sender
{
    [self performSegueWithIdentifier:@"selectHumor" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"selectHumor"])
    {
        humorSelectionViewController *svc = (humorSelectionViewController *)segue.destinationViewController;
        svc.myDelegate = self;
    }
}

-(void) secondViewControllerDismissed:(float)xScore withY:(float)yScore
{
    //POSSO SALVARE LE PREFERENZE direttamente nella tabella GENERE dove ho i top generi
    /*NSMutableArray *listaMusic = [[NSMutableArray alloc] init];
    NSMutableDictionary *libreria = [[NSMutableDictionary alloc] init];
    MPMediaItem *song = [songsArray objectAtIndex:0];
    NSString *tit = [song valueForProperty:MPMediaItemPropertyTitle];
    [libreria setObject:tit forKey:@"Titolo"];
    [libreria setObject:[NSNumber numberWithFloat:xScore] forKey:@"xScore"];
    [libreria setObject:[NSNumber numberWithFloat:yScore] forKey:@"yScore"];
    [listaMusic addObject:libreria];
    [MuudyDatabaseManager updateScore:listaMusic];*/
    [self reloadSongsWithXScore:xScore andYScore:yScore];
}

-(void) reloadSongsWithXScore:(float)xScore andYScore:(float)yScore
{
    
    NSString *query =[NSString stringWithFormat:@"SELECT s.Titolo, s.Artist FROM Song s WHERE ScoreX < %f + 0.2 AND ScoreX > %f - 0.2 AND ScoreY < %f + 0.2 AND ScoreY > %f - 0.2 ",xScore,xScore,yScore,yScore];
    NSArray *arrayQuery = [[NSArray alloc] initWithObjects:@"Titolo",@"Artista",nil];
    NSArray *tempSong = [[NSArray alloc]initWithArray:[MuudyDatabaseManager recoverDataFromDb:query :arrayQuery]];
    if(tempSong!=nil && tempSong.count!=0)
    {
        [appSoundPlayer stop];
        // create temporary autoreleased mutable array
        NSMutableArray *tmpArray = [NSMutableArray arrayWithCapacity:[tempSong count]];
        
        for (id anObject in tempSong)
        {
            NSUInteger randomPos = arc4random()%([tmpArray count]+1);
            [tmpArray insertObject:anObject atIndex:randomPos];
        }
        [[MuudyIpodHandler sharedInstance]getSongs];
        songs = [NSArray arrayWithArray:tmpArray];
        tmpArray = nil;
        tempSong = nil;
        ind = 0;
        NSInteger p = [songs count];
        labelCanzoniTot.text = [NSString stringWithFormat:@"%li", (long)p];
        [self createPlayList];
    }
    else
    {
        [TSMessage showNotificationInViewController:self title:@"Muudy" subtitle:@"Hai selezionato una zona senza musica" type:TSMessageNotificationTypeError duration:2.0 canBeDismissedByUser:YES];
    }
}

@end

