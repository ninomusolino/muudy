//
//  SyncViewController.m
//  Muudy
//
//  Created by Antonino Francesco Musolino on 19/07/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import "SyncViewController.h"
#import "SVProgressHUD.h"
#import "MuudyDatabaseManager.h"

@interface SyncViewController ()

@end

#define ARC4RANDOM_MAX      0x100000000

@implementation SyncViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(progressViewIncrement:) name:@"incrementaProgressView" object:nil];
    // Do any additional setup after loading the view.
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"incrementaProgressView" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    [self executeSync];
    });
}

-(void) executeSync
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD showWithStatus:@"Effettuo il Resync della tua libreria"];
    });
    NSString *query = @"Delete from Song";
    [MuudyDatabaseManager executeQuery:query];
    
    NSArray* songsArray = [MuudyDatabaseManager loadMusicDatabase];
    if (songsArray.count!=0)
    {
    NSMutableArray *listaMusic = [[NSMutableArray alloc] init];
    NSLog(@"ARRAYSONG COUNT = %lu",(unsigned long)[songsArray count]);
    unsigned long tot = [songsArray count]*2;
    for (int k=0; k<[songsArray count]; k++)
    {
        NSMutableDictionary *libreria = [[NSMutableDictionary alloc] init];
        MPMediaItem *song = [songsArray objectAtIndex:k];
        [libreria setObject:[song valueForProperty:MPMediaItemPropertyTitle] forKey:@"Titolo"];
        
        if ([song valueForProperty:MPMediaItemPropertyArtist]!=NULL)
            [libreria setObject:[song valueForProperty:MPMediaItemPropertyArtist] forKey:@"Artista"];
        else
            [libreria setObject:@"Unknown" forKey:@"Artista"];
        
        //[libreria setObject:[song valueForProperty:MPMediaItemPropertyArtist] forKey:@"Artista"];
        if ([song valueForProperty: MPMediaItemPropertyGenre]!=NULL)
            [libreria setObject:[song valueForProperty: MPMediaItemPropertyGenre] forKey:@"Genere"];
        else
            [libreria setObject:@"Genere Sconosciuto" forKey:@"Genere"];
        
        
        if ([song valueForProperty: MPMediaItemPropertyAlbumTitle]!=NULL)
            [libreria setObject:[song valueForProperty: MPMediaItemPropertyAlbumTitle] forKey:@"Album"];
        else
            [libreria setObject:@"Album Sconosciuto" forKey:@"Album"];
        
        /*if ([[song valueForProperty: MPMediaItemPropertyAlbumTitle] isEqualToString:@""])
         [libreria setObject:@"Album Sconosciuto" forKey:@"Album"];
         else
         [libreria setObject:[song valueForProperty: MPMediaItemPropertyAlbumTitle] forKey:@"Album"]; */
        
        [listaMusic addObject:libreria];
        NSNumber *progr = [[NSNumber alloc] initWithFloat:((float)k /(float)tot)];
        [self performSelectorOnMainThread:@selector(aumentaProgress:) withObject:progr waitUntilDone:YES];
    }
    [MuudyDatabaseManager insertSongInit:listaMusic];
    //MAGARI INSERIRE UN SEMAFORO
    
    [self resyncGenre];
    
    [self resyncArtist];
    
    [self populateEmptySong];
    dispatch_async(dispatch_get_main_queue(), ^{
    [SVProgressHUD dismiss];
    });
    [self performSegueWithIdentifier:@"endSync" sender:self];
    }
    else
    {
        [SVProgressHUD showErrorWithStatus:@"Non sono stati trovati brani nella tua libreria"];
    }
}

-(void)resyncGenre
{
    NSString *query = [NSString stringWithFormat:@"SELECT g.Genere,g.ScoreX,g.ScoreY FROM Genere g" ];
    NSArray *arrayQuery = [[NSArray alloc] initWithObjects:@"Genere",@"ScoreX",@"ScoreY",nil];
    NSArray *set = [MuudyDatabaseManager recoverDataFromDb:query :arrayQuery];
    for (int k=0; k<[set count]; k++)
    {

        NSMutableDictionary *dictVal = [set objectAtIndex:k];
        float xScore = [[dictVal objectForKey:@"ScoreX"] floatValue];
        float yScore = [[dictVal objectForKey:@"ScoreY"] floatValue];
        NSString * gen = [dictVal objectForKey:@"Genere"];
        NSString *query = [NSString stringWithFormat:@"UPDATE Song SET ScoreX = %f , ScoreY = %f WHERE Genre = '%@'",xScore,yScore,gen];
        
        dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        dispatch_sync(concurrentQueue,^{ [MuudyDatabaseManager executeQuery:query];});
    }
}

-(void)resyncArtist
{
    NSString *query = [NSString stringWithFormat:@"SELECT s.Artista,s.ScoreX,s.ScoreY FROM Artista s"];
    NSArray *arrayQuery = [[NSArray alloc] initWithObjects:@"Artista",@"ScoreX",@"ScoreY",nil];
    NSArray *set = [MuudyDatabaseManager recoverDataFromDb:query :arrayQuery];
    for (int k=0; k<[set count]; k++)
    {
        NSMutableDictionary *dictVal = [set objectAtIndex:k];
        float xScore = [[dictVal objectForKey:@"ScoreX"] floatValue];
        float yScore = [[dictVal objectForKey:@"ScoreY"] floatValue];
        NSString * art = [dictVal objectForKey:@"Artista"];
        NSString *query = [NSString stringWithFormat:@"UPDATE Song SET ScoreX = %f , ScoreY = %f WHERE Genre = '%@'",xScore,yScore,art];
        dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        dispatch_sync(concurrentQueue,^{ [MuudyDatabaseManager executeQuery:query];});
    }
}

-(void)populateEmptySong
{
    NSString *query = @"SELECT s.Artist FROM Song s WHERE s.ScoreX = 0 AND s.ScoreY = 0";
    NSArray *tableData = [[NSArray alloc] initWithObjects:@"Artist",nil];
    NSArray *noScore = [MuudyDatabaseManager recoverDataFromDb:query :tableData];
    NSLog(@"%lu",(unsigned long)noScore.count);
    for (int i=0; i<noScore.count; i++)
    {
        float x = floorf(((float)arc4random() / ARC4RANDOM_MAX) * 320.0f);
        float y = floorf(((float)arc4random() / ARC4RANDOM_MAX) * 320.0f);
        NSString *temp = [[noScore objectAtIndex:i]objectForKey:@"Artist"];
        NSString *titol = [temp stringByReplacingOccurrencesOfString:@"'" withString:@" "];
        NSString *query = [NSString stringWithFormat:@"UPDATE Song SET ScoreX = %f , ScoreY = %f WHERE Artist = '%@'",x,y,titol];
        NSLog(@"%@",query);
        [MuudyDatabaseManager executeQuery:query];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)endResync:(id)sender
{
    [self performSegueWithIdentifier:@"endSync" sender:self];
}

-(void)aumentaProgress:(NSNumber *)progre
{
    float d = [progre floatValue];
    float c = [progress progress]+d;
    [progress setProgress:c animated:YES];
    
}

-(void)progressViewIncrement:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:@"incrementaProgressView"])
    {
        NSNumber *prova = notification.object;
        
        [self performSelectorOnMainThread:@selector(aumentaProgress:) withObject:prova waitUntilDone:YES];
    }
}

@end
