//
//  AppDelegate.h
//  Muudy
//
//  Created by Antonino Francesco Musolino on 08/05/13.
//  Copyright (c) 2013 Antonino Francesco Musolino. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
