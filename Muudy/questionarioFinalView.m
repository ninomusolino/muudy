//
//  questionarioFinalView.m
//  Muudy
//
//  Created by Antonino Francesco Musolino on 11/03/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import "questionarioFinalView.h"
#import "SVProgressHUD.h"
@interface questionarioFinalView ()

@end

#define ARC4RANDOM_MAX      0x100000000

@implementation questionarioFinalView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.navigationItem.hidesBackButton = YES;
    [super viewDidLoad];
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_sync(concurrentQueue,^{ [self populateEmptySong];});
    // Do any additional setup after loading the view.
}


-(void)populateEmptySong
{
    dispatch_async(dispatch_get_main_queue(), ^{
       [SVProgressHUD showErrorWithStatus:@"Aggiorno il Database"];
    });
    NSString *query = @"SELECT s.Artist FROM Song s WHERE s.ScoreX = 0 AND s.ScoreY = 0";
    tableData = [[NSArray alloc] initWithObjects:@"Artist",nil];
    noScore = [MuudyDatabaseManager recoverDataFromDb:query :tableData];
    NSLog(@"%lu",(unsigned long)noScore.count);
    for (int i=0; i<noScore.count; i++)
    {
        float x = floorf(((float)arc4random() / ARC4RANDOM_MAX) * 320.0f);
        float y = floorf(((float)arc4random() / ARC4RANDOM_MAX) * 320.0f);
        NSString *temp = [[noScore objectAtIndex:i]objectForKey:@"Artist"];
        NSString *titol = [temp stringByReplacingOccurrencesOfString:@"'" withString:@" "];
        NSString *query = [NSString stringWithFormat:@"UPDATE Song SET ScoreX = %f , ScoreY = %f WHERE Artist = '%@'",x,y,titol];
        NSLog(@"%@",query);
        [MuudyDatabaseManager executeQuery:query];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)endQuestionario:(id)sender
{
    NSDate *lastSync = [[MPMediaLibrary defaultMediaLibrary] lastModifiedDate];
    [[NSUserDefaults standardUserDefaults] setObject:lastSync forKey:@"lastSync"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"RanBefore"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self performSegueWithIdentifier:@"endQuestionario" sender:self];
}

@end
