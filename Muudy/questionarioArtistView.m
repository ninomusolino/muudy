//
//  questionarioArtistView.m
//  Muudy
//
//  Created by Antonino Francesco Musolino on 11/05/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import "questionarioArtistView.h"
#import "SVProgressHUD.h"
@interface questionarioArtistView ()

@end

@implementation questionarioArtistView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideActivityView:) name:@"hideActivityView" object:nil];
    NSString *query = @"SELECT a.Artista FROM Artista a";
    tableData = [[NSArray alloc] initWithObjects:@"Artista",nil];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    artist = [MuudyDatabaseManager recoverDataFromDb:query :tableData];
    self.navigationItem.hidesBackButton = YES;
    [_tableView reloadData];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)next:(id)sender
{
    [self performSegueWithIdentifier:@"next" sender:self];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [artist count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [_tableView
                             dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if(cell==nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        [cell setBackgroundColor:[UIColor colorWithRed:.8 green:.8 blue:1 alpha:1]];
    }
    
    cell.textLabel.text = [[artist objectAtIndex:indexPath.row] objectForKey:@"Artista"];
    //cell.imageView.image = [UIImage imageNamed:@"itunes10_512_circle.png"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    art = [[artist objectAtIndex:indexPath.row] objectForKey:@"Artista"];
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"selectHumor" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"selectHumor"])
    {
        humorSelectionViewController *svc = (humorSelectionViewController *)segue.destinationViewController;
        svc.myDelegate = self;
    }
}


-(void) secondViewControllerDismissed:(float)xScore withY:(float)yScore
{
    
    NSString *query = [NSString stringWithFormat:@"SELECT s.Titolo,s.ScoreX,s.ScoreY FROM Song s WHERE Artist = '%@'",art];
    NSArray *arrayQuery = [[NSArray alloc] initWithObjects:@"Titolo",@"ScoreX",@"ScoreY",nil];
    NSArray *set = [MuudyDatabaseManager recoverDataFromDb:query :arrayQuery];
    NSMutableArray *listaMusic = [[NSMutableArray alloc] init];
    for (int k=0; k<[set count]; k++)
    {
        NSMutableDictionary *libreria = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *dictVal = [set objectAtIndex:k];
        float scoX = [[dictVal objectForKey:@"ScoreX"] floatValue];
        float scoY = [[dictVal objectForKey:@"ScoreY"] floatValue];
        NSString * tit = [dictVal objectForKey:@"Titolo"];
        [libreria setObject:tit forKey:@"Titolo"];
        [libreria setObject:art forKey:@"Artista"];
        if (scoX != 0 || scoY != 0)
        {
            xScore = (xScore + scoX)/2;
            yScore = (yScore + scoY)/2;
            [libreria setObject:[NSNumber numberWithFloat:xScore] forKey:@"xScore"];
            [libreria setObject:[NSNumber numberWithFloat:yScore] forKey:@"yScore"];
        }
        else
        {
            [libreria setObject:[NSNumber numberWithFloat:xScore] forKey:@"xScore"];
            [libreria setObject:[NSNumber numberWithFloat:yScore] forKey:@"yScore"];
        }
        [listaMusic addObject:libreria];
    }
    dispatch_queue_t concurrentQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_sync(concurrentQueue,^{ [MuudyDatabaseManager updateArtist:listaMusic.self];});
    //POSSO SALVARE LE PREFERENZE direttamente nella tabella Artista dove ho i top Artisti
    dispatch_sync(concurrentQueue,^{
        NSString *query2 = [NSString stringWithFormat:@"UPDATE Artista SET ScoreX = %f , ScoreY = %f WHERE Artista = '%@'",xScore,yScore,art];
        [MuudyDatabaseManager executeQuery:query2]; });


}

@end
