//
//  HomeViewController.m
//  Muudy
//
//  Created by Antonino Francesco Musolino on 11/03/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import "HomeViewController.h"
#import "iPodView.h"
#import "TSMessage.h"
#import "SVProgressHUD.h"

@interface HomeViewController ()

@end

@implementation HomeViewController
@synthesize songs;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    NSString *str = @"0.785";
    gradi45 = [str floatValue];
    posImg1 = 0;
    posImg2 = 5.495;
    posImg3 = 4.71;
    posImg4 = 3.925;
    posImg5 = 3.14;
    posImg6 = 2.355;
    posImg7 = 1.57;
    posImg8 = 0.785;
    animCounter = 0;
    [self resetIMG];
    [self animazione45];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
    image.userInteractionEnabled = YES;
    [image addGestureRecognizer:tap];
    /*[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notification_iPodLibraryDidChange:) name: MPMediaLibraryDidChangeNotification object:nil];
    
    [[MPMediaLibrary defaultMediaLibrary] beginGeneratingLibraryChangeNotifications];*/
}


-(void)viewDidAppear:(BOOL)animated
{

    NSDate *oldSync = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastSync"];
    NSLog(@"%@",oldSync);
    NSDate *lastSync = [[MPMediaLibrary defaultMediaLibrary] lastModifiedDate];
    if ([oldSync compare:lastSync] == NSOrderedDescending)
    {
        NSLog(@"date1 is later than date2");
    }
    else if ([oldSync compare:lastSync] == NSOrderedAscending)
    {
        [[NSUserDefaults standardUserDefaults] setObject:lastSync forKey:@"lastSync"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"date1 is earlier than date2");
        [self doResync];
    } else {
        NSLog(@"dates are the same");
        
    }
}

-(void)doResync
{
    NSLog(@"fafo suca");
    [self performSegueWithIdentifier:@"sync" sender:self];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)resetIMG{
    transRot = 0;
    img1.transform = CGAffineTransformMakeRotation(posImg1);
    img2.transform = CGAffineTransformMakeRotation(posImg2);
    img3.transform = CGAffineTransformMakeRotation(posImg3);
    img4.transform = CGAffineTransformMakeRotation(posImg4);
    img5.transform = CGAffineTransformMakeRotation(posImg5);
    img6.transform = CGAffineTransformMakeRotation(posImg6);
    img7.transform = CGAffineTransformMakeRotation(posImg7);
    img8.transform = CGAffineTransformMakeRotation(posImg8);
}

-(void)animazione45{
    if (animCounter == 0) {
        posImg1 = 0;
        posImg2 = 5.495;
        posImg3 = 4.71;
        posImg4 = 3.925;
        posImg5 = 3.14;
        posImg6 = 2.355;
        posImg7 = 1.57;
        posImg8 = 0.785;
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration: 0.05];
    img1.transform = CGAffineTransformMakeRotation(posImg1);
    img2.transform = CGAffineTransformMakeRotation(posImg2);
    img3.transform = CGAffineTransformMakeRotation(posImg3);
    img4.transform = CGAffineTransformMakeRotation(posImg4);
    img5.transform = CGAffineTransformMakeRotation(posImg5);
    img6.transform = CGAffineTransformMakeRotation(posImg6);
    img7.transform = CGAffineTransformMakeRotation(posImg7);
    img8.transform = CGAffineTransformMakeRotation(posImg8);
    [UIView commitAnimations];
    if (animCounter<16) {
        if (animCounter < 8) {
            posImg1 = posImg1 + gradi45;
            posImg2 = posImg2 + gradi45;
            posImg3 = posImg3 + gradi45;
            posImg4 = posImg4 + gradi45;
            posImg5 = posImg5 + gradi45;
            posImg6 = posImg6 + gradi45;
            posImg7 = posImg7 + gradi45;
            posImg8 = posImg8 + gradi45;
            //NSLog(@"%@%i", @"animCounter = ", animCounter);
        }else if(animCounter ==8){
            posImg1 = 0;
            posImg2 = posImg2 + gradi45;
            posImg3 = posImg3 + gradi45;
            posImg4 = posImg4 + gradi45;
            posImg5 = posImg5 + gradi45;
            posImg6 = posImg6 + gradi45;
            posImg7 = posImg7 + gradi45;
            posImg8 = posImg8 + gradi45;
        }else if(animCounter ==9){
            posImg1 = 0;
            posImg2 = 0;
            posImg3 = posImg3 + gradi45;
            posImg4 = posImg4 + gradi45;
            posImg5 = posImg5 + gradi45;
            posImg6 = posImg6 + gradi45;
            posImg7 = posImg7 + gradi45;
            posImg8 = posImg8 + gradi45;
        }else if(animCounter ==10){
            posImg1 = 0;
            posImg2 = 0;
            posImg3 = 0;
            posImg4 = posImg4 + gradi45;
            posImg5 = posImg5 + gradi45;
            posImg6 = posImg6 + gradi45;
            posImg7 = posImg7 + gradi45;
            posImg8 = posImg8 + gradi45;
        }else if(animCounter ==11){
            posImg1 = 0;
            posImg2 = 0;
            posImg3 = 0;
            posImg4 = 0;
            posImg5 = posImg5 + gradi45;
            posImg6 = posImg6 + gradi45;
            posImg7 = posImg7 + gradi45;
            posImg8 = posImg8 + gradi45;
        }else if(animCounter ==12){
            posImg1 = 0;
            posImg2 = 0;
            posImg3 = 0;
            posImg4 = 0;
            posImg5 = 0;
            posImg6 = posImg6 + gradi45;
            posImg7 = posImg7 + gradi45;
            posImg8 = posImg8 + gradi45;
        }else if(animCounter ==13){
            posImg1 = 0;
            posImg2 = 0;
            posImg3 = 0;
            posImg4 = 0;
            posImg5 = 0;
            posImg6 = 0;
            posImg7 = posImg7 + gradi45;
            posImg8 = posImg8 + gradi45;
        }else if(animCounter ==14){
            posImg1 = 0;
            posImg2 = 0;
            posImg3 = 0;
            posImg4 = 0;
            posImg5 = 0;
            posImg6 = 0;
            posImg7 = 0;
            posImg8 = posImg8 + gradi45;
        }else if(animCounter ==15){
            posImg1 = 0;
            posImg2 = 0;
            posImg3 = 0;
            posImg4 = 0;
            posImg5 = 0;
            posImg6 = 0;
            posImg7 = 0;
            posImg8 = 0;
        }
        [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(animazione45) userInfo:nil repeats:NO];
        animCounter = animCounter + 1;
    }else{
        animCounter = 0;
    }
}

-(IBAction)riproduciMusica:(id)sender
{
    [self performSegueWithIdentifier:@"playMusic" sender:self];
}

- (void )imageTapped:(UITapGestureRecognizer *) gestureRecognizer
{
    CGPoint point =[gestureRecognizer locationInView:imageContainer];
    //    NSLog(@"COORDINATA X = %f",  point.x);
    //    NSLog(@"COORDINATA Y = %f",  point.y);
    float xScore = ((point.x * 2)/image.bounds.size.width)-1;
    float yScore = ((point.y * 2)/image.bounds.size.height)-1;
    yScore = -yScore;
    NSString *query =[NSString stringWithFormat:@"SELECT s.Titolo, s.Artist FROM Song s WHERE ScoreX < %f + 0.2 AND ScoreX > %f - 0.2 AND ScoreY < %f + 0.2 AND ScoreY > %f - 0.2 ",xScore,xScore,yScore,yScore];
    NSArray *arrayQuery = [[NSArray alloc] initWithObjects:@"Titolo",@"Artista",nil];
    songs = [MuudyDatabaseManager recoverDataFromDb:query :arrayQuery];
    if ([songs count]==0)
    {
        [TSMessage showNotificationInViewController:self title:@"Muudy" subtitle:@"Hai selezionato una zona senza musica" type:TSMessageNotificationTypeError duration:2.0 canBeDismissedByUser:YES];
        
        //[SVProgressHUD showErrorWithStatus:@"Hai selezionato una zona senza musica"];
    }
    else
    {
        [self shuffleSongs:songs];
        [self performSegueWithIdentifier:@"playMusic" sender:self];
    }
}

-(void)shuffleSongs:(NSArray *)song
{
    // create temporary autoreleased mutable array
	NSMutableArray *tmpArray = [NSMutableArray arrayWithCapacity:[song count]];
    
	for (id anObject in song)
	{
		NSUInteger randomPos = arc4random()%([tmpArray count]+1);
		[tmpArray insertObject:anObject atIndex:randomPos];
	}
    songs = nil;
    songs = [NSArray arrayWithArray:tmpArray];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"playMusic"])
    {
        // Setup the destination ViewController
        iPodView *cvc = segue.destinationViewController;
        cvc.songs = songs;
    }
}

-(IBAction)backToIpod:(id)sender
{
    [self performSegueWithIdentifier:@"back" sender:self];
}

-(IBAction)resetDB:(id)sender
{
    //percorso file su cartella documents
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [documentPaths objectAtIndex:0];
    NSString *path = [documentsDir stringByAppendingPathComponent:@"muudy.sqlite"];
    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"RanBefore"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [self performSegueWithIdentifier:@"reset" sender:self];
}

@end
