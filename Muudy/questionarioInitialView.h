//
//  questionarioViewController.h
//  Muudy
//
//  Created by Antonino Francesco Musolino on 11/03/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface questionarioInitialView : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *nextButton;
-(IBAction)next:(id)sender;

@end
