//
//  SyncViewController.h
//  Muudy
//
//  Created by Antonino Francesco Musolino on 19/07/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SyncViewController : UIViewController
{
    IBOutlet UIProgressView     *progress;
}
-(IBAction)endResync:(id)sender;
@end
