//
//  questionarioArtistView.h
//  Muudy
//
//  Created by Antonino Francesco Musolino on 11/05/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MuudyDatabaseManager.h"
#import "humorSelectionViewController.h"


@interface questionarioArtistView : UIViewController<UITableViewDelegate, UITableViewDataSource,SecondDelegate>
{
    NSArray                     *artist;
    NSArray                     *tableData;
    NSString                    *art;
}

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
-(IBAction)next:(id)sender;
@end
