//
//  MuudyIpodHandler.m
//  Muudy
//
//  Created by Antonino Musolino on 07/09/15.
//  Copyright (c) 2015 Antonino Francesco Musolino. All rights reserved.
//

#import "MuudyIpodHandler.h"

@interface MuudyIpodHandler()
{
    NSArray *songs;
    int index;
}
@end

@implementation MuudyIpodHandler

+(MuudyIpodHandler*)sharedInstance
{
    static  MuudyIpodHandler* sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init])
    {
        index = 0;
        songs = [NSArray alloc];
    }
    return self;
}

-(void)setArrayWithSongs:(NSArray*)song
{
    songs = nil;
    songs = [NSArray arrayWithArray:song];
}

-(NSArray*)getSongs
{
    return songs;
}

@end