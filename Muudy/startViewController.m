//
//  startViewController.m
//  Muudy
//
//  Created by Antonino Francesco Musolino on 09/02/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import "startViewController.h"


@interface startViewController ()

@end

@implementation startViewController
@synthesize progress;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(progressViewIncrement:) name:@"incrementaProgressView" object:prova];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doInitial:) name:@"vaiQuest" object:nil];
    
    prova = 0;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"incrementaProgressView" object:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    BOOL ranBefore = [[NSUserDefaults standardUserDefaults] boolForKey:@"RanBefore"];
    progress.hidden = YES;
    //DA INSERIRE IL FATTO CHE DEVO RIFARE IL QUESTIONARIO SE HO SINCRONIZZATO

        if (!ranBefore)
        {
            
           // [self saveDbLocaly];
            [self dataBaseConnect];
        }
        else
        {
            [self performSegueWithIdentifier:@"normalUse" sender:self];
        }
}

-(void)progressViewIncrement:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:@"incrementaProgressView"])
    {
        prova = notification.object;
        
        [self performSelectorOnMainThread:@selector(aumentaProgress:) withObject:prova waitUntilDone:YES];
    }
}

-(void)doInitial:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:@"vaiQuest"])
    {
        [self performSegueWithIdentifier:@"questionario" sender:self];
    }
}

#pragma mark FUNZIONI DI INSERIMENTO INIZIALE DATI

-(void)dataBaseConnect
{
    songsArray = [MuudyDatabaseManager loadMusicDatabase];
    //NSMutableArray *listaMusic = [[NSMutableArray alloc] init];
    NSLog(@"ARRAYSONG COUNT = %lu",(unsigned long)[songsArray count]);
    unsigned long tot = [songsArray count];
    for (int k=0; k<[songsArray count]; k++)
    {
        MPMediaItem *song = [songsArray objectAtIndex:k];
        
        NSMutableDictionary *libreria = [[NSMutableDictionary alloc] init];
        
        [libreria setObject:[song valueForProperty:MPMediaItemPropertyTitle] forKey:@"Titolo"];
        
        if ([song valueForProperty:MPMediaItemPropertyArtist]!=NULL)
            [libreria setObject:[song valueForProperty:MPMediaItemPropertyArtist] forKey:@"Artista"];
        else
            [libreria setObject:@"Unknown" forKey:@"Artista"];
        
        //[libreria setObject:[song valueForProperty:MPMediaItemPropertyArtist] forKey:@"Artista"];
        if ([song valueForProperty: MPMediaItemPropertyGenre]!=NULL)
            [libreria setObject:[song valueForProperty: MPMediaItemPropertyGenre] forKey:@"Genere"];
        else
            [libreria setObject:@"Genere Sconosciuto" forKey:@"Genere"];
        
        
        if ([song valueForProperty: MPMediaItemPropertyAlbumTitle]!=NULL)
            [libreria setObject:[song valueForProperty: MPMediaItemPropertyAlbumTitle] forKey:@"Album"];
        else
            [libreria setObject:@"Album Sconosciuto" forKey:@"Album"];
        
        /*if ([[song valueForProperty: MPMediaItemPropertyAlbumTitle] isEqualToString:@""])
         [libreria setObject:@"Album Sconosciuto" forKey:@"Album"];
         else
         [libreria setObject:[song valueForProperty: MPMediaItemPropertyAlbumTitle] forKey:@"Album"]; */
        
        //[listaMusic addObject:libreria];
        
        NSNumber *progr = [[NSNumber alloc] initWithFloat:((float)k /(float)tot)];
        [self performSelectorOnMainThread:@selector(aumentaProgress:) withObject:progr waitUntilDone:YES];
    }
    NSLog(@"FINE COPIA");
    [self performSegueWithIdentifier:@"questionario" sender:self];
    
}

#pragma mark FUNZIONI PER INSERIMENTO E SELECT


-(NSArray *)dataBaseGetter:(NSString *)query :(NSArray *)arrayQuery
{
    NSArray *timeArray = [MuudyDatabaseManager recoverDataFromDb:query :arrayQuery];
    NSLog(@"RISULTATO SELECT SIZE %lu",(unsigned long)[timeArray count]);
    return timeArray;
}

#pragma mark SETUP APP_______________________________

-(void)saveDbLocaly
{
    //percorso file su cartella documents
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [documentPaths objectAtIndex:0];
    NSString *path = [documentsDir stringByAppendingPathComponent:@"muudy.sqlite"];
    //controllo se il file esiste
    if(![[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        //se non esiste lo copio nella cartella dosuments
		NSString *pathLocale=[[NSBundle mainBundle] pathForResource:@"muudy" ofType:@"sqlite"];
		if ([[NSFileManager defaultManager] copyItemAtPath:pathLocale toPath:path error:nil] == YES)
		{
			NSLog(@"copia eseguita");
            [self performSelectorInBackground:@selector(dataBaseConnect) withObject:nil];
            progress.hidden = NO;
		}
    }
    else
        [self performSegueWithIdentifier:@"questionario" sender:self];
}

#pragma mark FUNZIONI DI GRAFICA

-(void)aumentaProgress:(NSNumber *)progre
{
    float d = [progre floatValue];
    float c = [progress progress]+d;
    [progress setProgress:c animated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
