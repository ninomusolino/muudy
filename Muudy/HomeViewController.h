//
//  HomeViewController.h
//  Muudy
//
//  Created by Antonino Francesco Musolino on 11/03/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MuudyDatabaseManager.h"
#import "MuudyIpodHandler.h"

@interface HomeViewController : UIViewController
{
    IBOutlet UIImageView            *img1;
    IBOutlet UIImageView            *img2;
    IBOutlet UIImageView            *img3;
    IBOutlet UIImageView            *img4;
    IBOutlet UIImageView            *img5;
    IBOutlet UIImageView            *img6;
    IBOutlet UIImageView            *img7;
    IBOutlet UIImageView            *img8;
    float                           gradi45;
    float                           sommagradi;
    float                           transRot;
    float                           posImg1;
    float                           posImg2;
    float                           posImg3;
    float                           posImg4;
    float                           posImg5;
    float                           posImg6;
    float                           posImg7;
    float                           posImg8;
    int                             animCounter;
    IBOutlet    UIView              *imageContainer;
    IBOutlet    UIImageView         *image;
    NSArray                         *songs;
    dispatch_semaphore_t semaphore;
}

@property (nonatomic) NSArray *songs;

-(IBAction)backToIpod:(id)sender;
-(IBAction)resetDB:(id)sender;
@end
