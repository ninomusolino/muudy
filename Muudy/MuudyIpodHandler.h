//
//  MuudyIpodHandler.h
//  Muudy
//
//  Created by Antonino Musolino on 07/09/15.
//  Copyright (c) 2015 Antonino Francesco Musolino. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MuudyIpodHandler : NSObject
+(MuudyIpodHandler*)sharedInstance;
-(NSArray*)getSongs;
-(void)setArrayWithSongs:(NSArray*)songs;
@end
