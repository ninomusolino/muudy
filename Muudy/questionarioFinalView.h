//
//  questionarioFinalView.h
//  Muudy
//
//  Created by Antonino Francesco Musolino on 11/03/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MuudyDatabaseManager.h"

@interface questionarioFinalView : UIViewController
{
    NSArray                     *tableData;
    NSArray                     *noScore;
}
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
-(IBAction)endQuestionario:(id)sender;

@end
