//
//  startViewController.h
//  Muudy
//
//  Created by Antonino Francesco Musolino on 09/02/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MuudyDatabaseManager.h"

@interface startViewController : UIViewController
{
    IBOutlet UIProgressView     *progress;
    NSNumber                    *prova;
    NSArray                     *songsArray;
}

@property (nonatomic, retain)UIProgressView  *progress;
@end
