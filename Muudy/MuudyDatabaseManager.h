//
//  Data.h
//  Muudy
//
//  Created by Antonino Francesco Musolino on 12/05/13.
//  Copyright (c) 2013 Antonino Francesco Musolino. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import <MediaPlayer/MediaPlayer.h>

@interface MuudyDatabaseManager : NSObject
{
    NSMutableArray *lista;
}

- (unsigned)getSize;
- (id)objectAtIndex:(unsigned)theIndex;

/** Restituisce un array gli oggetti che soddisfano la query torna un array di dizionari
 @param query la query SQL
 @param arrayKey i campi della tabella e campi del dizionario che crea per ogni cella dell'array
 */
+ (NSArray *)recoverDataFromDb:(NSString *)query :(NSArray *)arrayKey;

/** Esegue la query in ingresso
 @param sqlAdd query SQL
*/
+ (int)executeQuery:(NSString *)query;

/** Inizializza il DB
 @param wrap contiene la libreria musicale
*/
+ (void)insertSongInit:(NSArray *) wrap;

/** Modifica il punteggio degli artisti
 @param listaM gli artisti da inserire
 */
+ (void)updateArtist:(NSArray *) listaM;

/** Fa l'update dei punteggi delle canzoni
 @param listaM contiene i valori X ed Y
 */
+(void)updateScore:(NSArray *)listaM;

/** Restituisce la collezione multimediale dell'utente
 */
+(NSArray *)loadMusicDatabase;

@property (nonatomic, retain) NSMutableArray *lista;


@end
