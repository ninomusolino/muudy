//
//  humorSelectionViewController.h
//  Muudy
//
//  Created by Antonino Francesco Musolino on 11/05/14.
//  Copyright (c) 2014 Antonino Francesco Musolino. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SecondDelegate <NSObject>
-(void) secondViewControllerDismissed:(float)xScore withY:(float)yScore;
@end


@interface humorSelectionViewController : UIViewController
{
    __unsafe_unretained id  myDelegate;
    float                       xScore;
    float                       yScore;
    IBOutlet    UIImageView         *image;
    IBOutlet    UIView              *imageContainer;
}

@property (nonatomic, assign) id<SecondDelegate>    myDelegate;
@property (nonatomic, assign) float                 scoreX;
@property (nonatomic, assign) float                 scoreY;
@property (nonatomic, retain) UIImageView           *image;
@property (nonatomic, retain) UIView                *imageContainer;

@end
